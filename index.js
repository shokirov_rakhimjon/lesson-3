// Lesson 3

function fizzBuzz () {
    for (var i=1; i <= 100; i++){
    if (i % 15 == 0)
        console.log("FizzBuzz");
    else if (i % 3 == 0)
        console.log("Fizz");
    else if (i % 5 == 0)
        console.log("Buzz");
    else
        console.log(i);
};
}


function filterArray (arr){
    let output = []
    for (let i=0; i<arr.length; i++){
        if (Array.isArray(arr[i])){
            output.push(arr[i])
        }
    }
    // console.log(output);
    let a = Array.from(new Set([].concat(...output)))
    // console.log(a);
    return a.sort();
}



// Output DON'T TOUCH!
// console.log(fizzBuzz())
console.log(filterArray([ [2], 23, 'dance', [7,7,7],true, [3, 5, 3, 1] ]))

